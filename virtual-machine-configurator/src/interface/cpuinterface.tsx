export interface ICpuInterface {
    label: string;
    price: number;
    value: number;
    id: number;
  }