export interface ICostInterface {
  title: string;
  cost: number;
  id: number;
}
