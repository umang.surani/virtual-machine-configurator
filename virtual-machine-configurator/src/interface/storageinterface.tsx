export interface IStorageInterface {
    type: string;
    volume: string;
    capacity: number;
    encryption: boolean;
    iops:number;
    backuprequired: boolean;
    remarks: string;
    isreview: boolean;
    isroot: boolean;
    id: number;
    price: number;
  }