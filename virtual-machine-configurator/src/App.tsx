import React from 'react'; 
import './App.css';
import MainContainer from './Containers/main/maincontainers';

function App() {
  return (
    <div className="App">
       <MainContainer/>
    </div>
  );
}

export default App;
