import { Box, Typography } from "@material-ui/core";
import React from "react";

interface TabProps {
    children?: React.ReactNode;
    dir?: string;
    index: any;
    value: any;
  }
export function TabPanel(props: TabProps) {
    const { children, value, index, ...other } = props;

    return (
        <div role="tabpanel" hidden={value !== index} id={`full-width-tabpanel-${index}`}
             aria-labelledby={`full-width-tab-${index}`} {...other}>
          {value === index && (
            <Box p={3}>
              <Typography>{children}</Typography>
            </Box>
          )}
        </div>
      );
}