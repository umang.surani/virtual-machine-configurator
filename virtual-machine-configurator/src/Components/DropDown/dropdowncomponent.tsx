import { TextField } from "@material-ui/core"
import React from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import './dropdown.css';

function DropdownComponent(props: any) {
  return (
    <TextField id="regions" select label={props.label} value={props.selectedItem}
      onChange={props.onSlectionChange} variant="outlined" className="dropdown" size="small"
      InputProps={{
        readOnly: props?.isreview,
      }}>
      {props.items.map((option: any) => (
        <MenuItem key={option.value} value={option.value}>
          {option.label}
        </MenuItem>
      ))}
    </TextField>
  );
}
export default DropdownComponent;