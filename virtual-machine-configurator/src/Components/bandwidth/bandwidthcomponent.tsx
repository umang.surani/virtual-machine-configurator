import "./bandwidth.css";

export function BandwidthComponent(prop: any) {
  return (
    <div className="bandwidthcard">
      <div className="bandwidthcontainer">
        <div className="title">
          <span className="colortitle">{prop?.bandwidth}</span>/Month
        </div>
      </div>
    </div>
  );
}
