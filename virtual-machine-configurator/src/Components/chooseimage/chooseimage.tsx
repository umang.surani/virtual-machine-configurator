import React, { useState } from "react";
import { ImagecardComponent } from "../card/imagecardcomponent";

const ImagesTypes = [
  {
    id: 1,
    name: "Linux 2 image",
    description:
      "Linux come with 5 years of supprot. It provide Linux kernel 4.14 tuned for optimal performance.",
    OsType: [
      {
        value: "x86",
        label: "64-bit (x86)",
        id: "li86",
        checked: true,
      },
      {
        value: "ARM",
        label: "64-bit (xARM)",
        id: "li64",
        checked: false,
      },
    ],
    price: 243.61,
    isSelecte: false,
    hidden: false,
    isreview: false,
  },
  {
    id: 2,
    name: "Ubuntu Server 18.04 LTS",
    description:
      "Linux come with 5 years of supprot. It provide Linux kernel 4.14 tuned for optimal performance.",
    OsType: [
      {
        value: "x86",
        label: "64-bit (x86)",
        id: "ub86",
        checked: true,
      },
      {
        value: "ARM",
        label: "64-bit (xARM)",
        id: "un64",
        checked: false,
      },
    ],
    price: 243.61,
    isSelecte: false,
    hidden: false,
    isreview: false,
  },
  {
    id: 3,
    name: "Red Hat Enterprise Linux 8",
    description:
      "Linux come with 5 years of supprot. It provide Linux kernel 4.14 tuned for optimal performance.",
    OsType: [
      {
        value: "x86",
        label: "64-bit (x86)",
        id: "rh86",
        checked: true,
      },
      {
        value: "ARM",
        label: "64-bit (xARM)",
        id: "rh64",
        checked: false,
      },
    ],
    price: 300.0,
    isSelecte: false,
    hidden: false,
    isreview: false,
  },
  {
    id: 4,
    name: "Microsoft Windows Server 2019 Base",
    description:
      "Linux come with 5 years of supprot. It provide Linux kernel 4.14 tuned for optimal performance.",
    OsType: [
      {
        value: "ARM",
        label: "64-bit (xARM)",
        id: "wi64",
      },
    ],
    price: 338.77,
    isSelecte: false,
    hidden: false,
    isreview: false,
  },
  {
    id: 5,
    name: "SUSE Linux Enterprise Server",
    description:
      "Linux come with 5 years of supprot. It provide Linux kernel 4.14 tuned for optimal performance.",
    OsType: [
      {
        value: "x86",
        label: "64-bit (x86)",
        id: "su86",
        checked: true,
      },
      {
        value: "ARM",
        label: "64-bit (xARM)",
        id: "su64",
        checked: false,
      },
    ],
    price: 200.22,
    isSelecte: false,
    hidden: false,
    isreview: false,
  },
];

export function ChooseImageComponent(prop: any) {
  const [ImageTypes, setImageTypes] = useState(ImagesTypes);
  let rows = [];

  const onclick = (ImageType: any) => {
    let updatelist: any[] = [];
    ImageTypes.forEach((val) => updatelist.push(Object.assign({}, val)));
    updatelist.forEach((val) => (val.isSelecte = false));
    const editIndex = updatelist.findIndex(
      (element) => element.id === ImageType.id
    );
    updatelist[editIndex].isSelecte = true;
    setImageTypes(updatelist);
    prop.onImageSlectionChange(updatelist[editIndex]);
  };

  const onSlectionChange = (OSType: any, id: number) => {
    let updatelist: any[] = [];
    ImageTypes.forEach((val) => updatelist.push(Object.assign({}, val)));
    const editIndex = updatelist.findIndex((element) => element.id === id);
    updatelist[editIndex].OsType.forEach((o: any) => (o.checked = false));
    const osIndex = updatelist[editIndex].OsType.findIndex(
      (e: any) => e.id === OSType.id
    );
    updatelist[editIndex].OsType[osIndex].checked = true;
    setImageTypes(updatelist);
  };

  for (let index = 0; index < ImageTypes.length; index++) {
    const element = ImageTypes[index];
    if (prop.selectedRegion === 1 || prop.selectedRegion === 2) {
      ImageTypes[index].hidden = false;
      rows.push(
        <ImagecardComponent
          isSummary={false}
          ImageType={element}
          key={index}
          onclick={onclick}
          onSlectionChange={onSlectionChange}
        />
      );
    } else {
      if (element.id === 4) {
        ImageTypes[index].hidden = true;
      }
      rows.push(
        <ImagecardComponent
          isSummary={false}
          ImageType={element}
          key={index}
          onclick={onclick}
          onSlectionChange={onSlectionChange}
        />
      );
    }
  }

  return <span>{rows}</span>;
}
