import "./imagecard.css";
import logo from "../../logo.svg";
import React from "react";
import { FormControl, Button } from "@material-ui/core";

export function ImagecardComponent(prop: any) {
  let rows = [];
  if (prop?.ImageType?.OsType?.length !== 1) {
    if (!prop?.isSummary) {
      for (let index = 0; index < prop?.ImageType?.OsType?.length; index++) {
        const element = prop?.ImageType?.OsType[index];
        rows.push(
          <span key={index}>
            <input
              type="radio"
              id={element.id}
              name={`ostype` + prop?.ImageType?.id}
              value={element.value}
              checked={element.checked}
              onChange={() =>
                prop?.onSlectionChange(element, prop?.ImageType?.id)
              }
            />
            <label htmlFor={element.id} className="radio">
              {element.label}
            </label>
            <br />
          </span>
        );
      }
    } else {
      for (let index = 0; index < prop?.ImageType?.OsType?.length; index++) {
        const element = prop?.ImageType?.OsType[index];
        if (element.checked) {
          rows.push(
            <span key={index}>
              <input
                type="radio"
                id={element.id}
                name={`ostype` + prop?.ImageType?.id}
                value={element.value}
                checked={element.checked}
                onChange={() =>
                  prop?.onSlectionChange(element, prop?.ImageType?.id)
                }
              />
              <label htmlFor={element.id} className="radio">
                {element.label}
              </label>
              <br />
            </span>
          );
        }
      }
    }
  } else {
    const element = prop?.ImageType?.OsType[0];
    rows.push(
      <span key="0">
        <label htmlFor={element.id} className="radio">
          {element.label}
        </label>
        <br />
      </span>
    );
  }

  return (
    <div className="card" hidden={prop?.ImageType?.hidden}>
      <div className="container">
        <div className="id">
          <img src={logo} className="App-logo" alt="logo" />
        </div>
        <div className="content">
          <span className="title">{prop?.ImageType?.name}</span>
          <span className="paregraph">{prop?.ImageType?.description}</span>
        </div>
        <div className="right-content">
          <FormControl component="fieldset">{rows}</FormControl>
          <span hidden={prop?.isSummary} className="spanclass">
            <Button
              variant="contained"
              color="primary"
              className="btnclass"
              onClick={() => prop?.onclick(prop?.ImageType)}
              disabled={prop?.ImageType.isSelecte}
            >
              Select
            </Button>
          </span>
        </div>
      </div>
    </div>
  );
}
