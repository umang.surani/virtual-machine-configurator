import "./instancecard.css";

export function InstanceCardComponent(prop: any) {
  return (
    <div className="instancecard">
      <div className="instancecontainer">
        <div className="title">{prop?.configurationLabel}</div>
        <div className="subcontent">
          <div>
            <span>{prop?.selectCore?.label} CPU</span><br/>
            <span>{prop?.selectMemory?.label} RAM</span><br/>
            <span>Moderate Netwrok Performance</span>
          </div>
          <div>512GB</div>
        </div>
      </div>
    </div>
  );
}
