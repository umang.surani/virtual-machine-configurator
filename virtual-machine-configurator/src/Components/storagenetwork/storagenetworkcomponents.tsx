import { Checkbox, TextField } from "@material-ui/core";
import { IconButton } from "@material-ui/core";
import CloseIcon from "@material-ui/icons/Close";
import React from "react";
import DropdownComponent from "../DropDown/dropdowncomponent";
import "./storagenetwork.css";

export function StorageNetworkComponent(prop: any) {
  return (
    <div className="storagenetwork">
      <div className="storagenetworkcard">
        <div className="storagenetworkcontainer">
          <div className="content">
            <span className="title">Type</span>
            <span className="paregraphdropdown">
              <DropdownComponent
                label=""
                isreview ={prop?.storage?.isreview}
                items={prop?.types}
                selectedItem={prop?.storage?.type}
                onSlectionChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                  prop?.onSlectionChange(e, prop?.storage, "type")
                }
              />
            </span>
          </div>
          <div className="content">
            <span className="title">Volume</span>
            <span className="paregraph">{prop?.storage?.volume}</span>
          </div>
          <div className="content">
            <span className="title">Capacity(GB)</span>
            <span
              className="paregraphdropdown"
              hidden={prop?.storage?.isreview}
            >
              <TextField 
               InputProps={{
                readOnly: prop?.storage?.isreview,
              }}
                id="outlined-size-small"
                defaultValue="120"
                variant="outlined"
                size="small"
                type="number"
                value={prop?.storage?.capacity}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                  prop?.onSlectionChange(e, prop?.storage, "capacity")
                }
              />
            </span>
          </div>
          <div className="content">
            <span className="title">Encryption</span>
            <span className="paregraph" hidden={prop?.storage?.isreview}>
              <Checkbox
                color="primary"
                checked={prop?.storage?.encryption}
                inputProps={{ "aria-label": "secondary checkbox" }}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                  prop?.onSlectionChange(e, prop?.storage, "encryption")
                }
              />
            </span>
          </div>
          <div className="content">
            <span className="title">IOPS</span>
            <span className="paregraph">{prop?.storage?.iops}</span>
          </div>
          <div className="content">
            <span className="title">Backup Required</span>
            <span className="paregraph" hidden={prop?.storage?.isreview}>
              <Checkbox
                checked={prop?.storage?.backuprequired}
                color="primary"
                inputProps={{ "aria-label": "secondary checkbox" }}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                  prop?.onSlectionChange(e, prop?.storage, "backuprequired")
                }
              />
            </span>
          </div>
          <div className="content">
            <span className="title">Remarks</span>
            <span className="paregraph" hidden={prop?.storage?.isreview}>
              <TextField
              InputProps={{
                readOnly: prop?.storage?.isreview,
              }}
                id="outlined-size-small12"
                variant="outlined"
                size="small"
                value={prop?.storage?.remarks}
                onChange={(e: React.ChangeEvent<HTMLInputElement>) =>
                  prop?.onSlectionChange(e, prop?.storage, "remarks")
                }
              />
            </span>
          </div>
        </div>
      </div>
      <span hidden={prop?.storage?.isroot}>
        <IconButton
          aria-label="delete"
          className="btnclass"
          onClick={() => prop?.onRemoveClick(prop?.storage)}
        >
          <CloseIcon fontSize="small" />
        </IconButton>
      </span>
    </div>
  );
}
