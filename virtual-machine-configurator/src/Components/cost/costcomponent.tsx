import "./cost.css";

export function CostComponent(prop: any) {
  let rows = [];
  for (let index = 0; index < prop?.costEstimate?.length; index++) {
    const element = prop?.costEstimate[index];
    rows.push(
      <span className="subcontent">
        <span className="paregraph">{element.title}</span>
        <span className="paregraph">${element.cost}</span>
      </span>
    );
  }
  return (
    <div className="costcard">
      <div className="containercost">
        <div className="title">Cost Estimates</div>
        <div className="content">{rows}</div>
        <div>
          <hr />
          <div className="right-content">${prop.totalPrice.toFixed(2)}/mo</div>
        </div>
      </div>
    </div>
  );
}
