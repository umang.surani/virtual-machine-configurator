import { Button, Slider, Tooltip, Typography } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import { StorageNetworkComponent } from "../../Components/storagenetwork/storagenetworkcomponents";
import { IStorageInterface } from "../../interface/storageinterface";
import "./storagenetwork.css";

const types = [
  {
    label: "Storage",
    value: "Storage",
  },
  {
    label: "SSD",
    value: "SSD",
  },
  {
    label: "Magnetic Disk",
    value: "Magnetic Disk",
  },
];

const marks = [
  {
    value: 256,
    label: "256GB",
  },
  {
    value: 2048,
    label: "2TB",
  },
];

function ValueLabelComponent(props: any) {
  const { children, open, value } = props;

  return (
    <Tooltip open={open} enterTouchDelay={0} placement="top" title={value}>
      {children}
    </Tooltip>
  );
}

export function StorageNetworkContainer(props: any) {
  const InitStorage = () => {
    let storageArray: IStorageInterface[] = [];
    let storageObj: IStorageInterface = {
      backuprequired: false,
      capacity: 150,
      encryption: true,
      iops: 600,
      isreview: false,
      isroot: true,
      remarks: "",
      type: "Storage",
      volume: "Root",
      id: 1,
      price: 40,
    };
    storageArray.push(storageObj);
    return storageArray;
  };

  const [storage, setStorage] = useState(InitStorage);
  const [values, setValues] = useState({
    type: "",
    volume: "",
    capacity: 0,
    encryption: false,
    iops: 0,
    backuprequired: false,
    remarks: "",
    isreview: false,
    isroot: false,
    id: 0,
  });
  const [disableBtn, setDisableBtn] = useState(true);
  const [bandwidth, setBandwidth] = useState(256);

  let rows = [];
  const onSlectionChange = (
    event: React.ChangeEvent<HTMLInputElement>,
    storageObj: IStorageInterface,
    propertyName: string
  ) => {
    if (storageObj?.id !== 0) {
      setValues(storageObj);
      const localValues =
        event.target.type === "checkbox"
          ? event.target.checked
          : event.target.value;
      setValues((oldValues) => ({ ...oldValues, [propertyName]: localValues }));
    }
  };

  useEffect(() => {
    let updatelist: IStorageInterface[] = [];
    storage.forEach((val) => updatelist.push(Object.assign({}, val)));
    const editIndex = updatelist.findIndex(
      (element) => element.id === values?.id
    );
    if (editIndex !== -1) {
      updatelist[editIndex].backuprequired = values?.backuprequired;
      updatelist[editIndex].capacity = values?.capacity;
      updatelist[editIndex].encryption = values?.encryption;
      updatelist[editIndex].remarks = values?.remarks;
      updatelist[editIndex].type = values?.type;
      if (updatelist[editIndex].capacity > 0) {
        if (updatelist[editIndex].capacity < 100) {
          updatelist[editIndex].iops = 100;
        } else if (
          updatelist[editIndex].capacity >= 100 &&
          updatelist[editIndex].capacity <= 500
        ) {
          updatelist[editIndex].iops = 600;
        } else {
          updatelist[editIndex].iops = 1000;
        }
      }
      if (updatelist[editIndex].type === "SSD") {
        updatelist[editIndex].price = 40;
      } else {
        updatelist[editIndex].price = 20;
      }
      setStorage(updatelist);
      if (
        updatelist[editIndex].capacity > 0 &&
        updatelist[editIndex].type !== "Storage"
      ) {
        setDisableBtn(false);
        props?.handleStorageNetworkChange(updatelist, false);
      } else if (
        updatelist[editIndex].capacity < 0 ||
        updatelist[editIndex].type === "Storage" ||
        updatelist[editIndex].capacity.toString() === ""
      ) {
        setDisableBtn(true);
      }
    }
  }, [values]);

  const onAddClick = () => {
    const id =
      Math.max.apply(
        Math,
        storage.map(function (o) {
          return o.id;
        })
      ) + 1;
    let storageObj: IStorageInterface = {
      backuprequired: false,
      capacity: 8,
      encryption: true,
      iops: 100,
      isreview: false,
      isroot: false,
      remarks: "",
      type: "Storage",
      volume: "Ext",
      id: id,
      price: 20,
    };
    let updatelist: IStorageInterface[] = [];
    storage.forEach((val) => updatelist.push(Object.assign({}, val)));
    updatelist.push(storageObj);
    setStorage(updatelist);
    setDisableBtn(true);
  };

  const onRemoveClick = (storageObj: IStorageInterface) => {
    const id = storageObj?.id;
    let updatelist: IStorageInterface[] = [];
    storage.forEach((val) => updatelist.push(Object.assign({}, val)));
    const index = updatelist.findIndex((e) => e.id === id);
    if (index !== -1) {
      updatelist.splice(index, 1);
    }
    setStorage(updatelist);
  };

  const valuetext = (value: number) => {
    setBandwidth(value);
    let unit,
      units = ["TB", "GB"];
    for (
      unit = units.pop();
      units.length && value >= 1024;
      unit = units.pop()
    ) {
      value /= 1024;
    }
    if (unit === "GB") {
      return `${value.toFixed()}${unit}`;
    }
    return `${value.toFixed(1)}${unit}`;
  };

  const onSlideChange = () => {
    props?.handleBandwidthChange(bandwidth);
  };

  for (let index = 0; index < storage.length; index++) {
    const element = storage[index];
    rows.push(
      <StorageNetworkComponent
        types={types}
        storage={element}
        key={index}
        onSlectionChange={onSlectionChange}
        onRemoveClick={onRemoveClick}
      />
    );
  }

  return (
    <div className="maincontain">
      <span>{rows}</span>
      <span className="btncalss">
        <Button
          variant="contained"
          color="primary"
          onClick={onAddClick}
          disabled={disableBtn}
        >
          Add Storage
        </Button>
      </span>
      <span>
        <h1>Netwrok Bandwidth Configuration</h1>
        <Typography id="discrete-slider-always" gutterBottom>
          Outboud Trafic
        </Typography>
        <Slider
          defaultValue={256}
          getAriaValueText={valuetext}
          aria-labelledby="discrete-slider-always"
          step={1}
          min={256}
          max={2048}
          marks={marks}
          ValueLabelComponent={ValueLabelComponent}
          valueLabelFormat={valuetext}
          valueLabelDisplay="auto"
          onChange={onSlideChange}
        />
      </span>
    </div>
  );
}
