import { Button } from "@material-ui/core";
import React from "react";
import { BandwidthComponent } from "../../Components/bandwidth/bandwidthcomponent";
import { ImagecardComponent } from "../../Components/card/imagecardcomponent";
import { InstanceCardComponent } from "../../Components/card/instancecardcomponent";
import { StorageNetworkComponent } from "../../Components/storagenetwork/storagenetworkcomponents";
import "./summary.css";

const types = [
  {
    label: "SSD",
    value: "SSD",
  },
  {
    label: "Magnetic Disk",
    value: "Magnetic Disk",
  },
];

export function SummaryContainer(prop: any) {
  let rows = [];

  const onclick = () => {};
  for (let index = 0; index < prop?.storageArray.length; index++) {
    const e = prop?.storageArray[index];
    rows.push(
      <StorageNetworkComponent
        types={types}
        storage={e}
        key={index}
        onSlectionChange={onclick}
        onRemoveClick={onclick}
      />
    );
  }

  return (
    <div>
      <div className="summarycontainer">
        <span className="title">Image</span>
        <span className="link">
          <Button
            variant="contained"
            size="small"
            color="primary"
            onClick={() => prop?.onEditClick(1)}
          >
            Edit
          </Button>
        </span>
      </div>
      <div>
        <ImagecardComponent
          ImageType={prop.selectedImage}
          onclick={onclick}
          isSummary={true}
        />
      </div>
      <div className="summarycontainer">
        <span className="title">Instance</span>
        <span className="link">
          <Button
            variant="contained"
            size="small"
            color="primary"
            onClick={() => prop?.onEditClick(2)}
          >
            Edit
          </Button>
        </span>
      </div>
      <div>
        <InstanceCardComponent {...prop} />
      </div>
      <div className="summarycontainer">
        <span className="title">Bandwidth</span>
        <span className="link">
          <Button
            variant="contained"
            size="small"
            color="primary"
            onClick={() => prop?.onEditClick(3)}
          >
            Edit
          </Button>
        </span>
      </div>
      <div>
        <BandwidthComponent {...prop} />
      </div>
      <div className="summarycontainer">
        <span className="title">Storage</span>
        <span className="link">
          <Button
            variant="contained"
            size="small"
            color="primary"
            onClick={() => prop?.onEditClick(3)}
          >
            Edit
          </Button>
        </span>
      </div>
      <div>{rows}</div>
    </div>
  );
}
