import { Button } from "@material-ui/core";
import React, { ChangeEvent, useState } from "react";
import { CostComponent } from "../../Components/cost/costcomponent";
import DropdownComponent from "../../Components/DropDown/dropdowncomponent";
import { ICostInterface } from "../../interface/costinterface";
import { ICpuInterface } from "../../interface/cpuinterface";
import { IStorageInterface } from "../../interface/storageinterface";
import { TabContainer } from "../tab/tabcontainer";
import "./main.css";

const regions = [
  {
    label: "us-east-1",
    value: 1,
  },
  {
    label: "us-east-2",
    value: 2,
  },
  {
    label: "us-west-1",
    value: 3,
  },
  {
    label: "india-1",
    value: 4,
  },
];

function MainContainer() {
  const CostModel: ICostInterface[] = [];
  const [selectedRegion, setSelectedRegion] = useState(1);
  const [costEstimate, setCostEstimate] = useState(CostModel);
  const [TotalPrice, setTotalPrice] = useState(0.0);
  const [selectedTab, setSelectedTab] = useState("1");
  const [processDisable, setProcessDisable] = useState(true);
  const [backHidden, setBackHidden] = useState(true);
  const [selectedImage, setSelectedImage] = useState();
  const [selectCore, setSelectCore] = useState<ICpuInterface>();
  const [selectMemory, setSelectMemory] = useState<ICpuInterface>();
  const [configurationLabel, setConfigurationLabel] = useState("");
  const [storageArray, setStorageArray] = useState<IStorageInterface[]>();
  const [bandwidth, setBandwidth] = useState("");

  const onSlectionChange = (event: ChangeEvent<HTMLInputElement>) => {
    const val: number = +event?.target?.value.toString();
    setSelectedRegion(val);
  };

  const setCostValue = (cost: ICostInterface) => {
    let updatelist: ICostInterface[] = [];
    costEstimate.forEach((val) => updatelist.push(Object.assign({}, val)));
    const editIndex = updatelist.findIndex((element) => element.id === cost.id);
    if (editIndex === -1) {
      updatelist.push(cost);
    } else {
      updatelist[editIndex] = cost;
    }
    let totalPrice = 0.0;
    updatelist.forEach((val) => (totalPrice += val.cost));
    setTotalPrice(totalPrice);
    setCostEstimate(updatelist);
    setProcessBtnAfterConfiuration(updatelist);
  };

  const onImageSlectionChange = (selectedImage: any) => {
    let imageCoast: ICostInterface = {
      title: selectedImage.name,
      cost: selectedImage.price,
      id: 1,
    };
    selectedImage.isreview = true;
    setCostValue(imageCoast);
    setProcessDisable(false);
    setSelectedImage(selectedImage);
  };

  const onProceedClick = () => {
    let tab = +selectedTab + 1;
    if (selectedTab === "4") {
      alert("You sucessfully cofig VM");
      return;
    }
    setSelectedTab(tab.toString());
    setProcessDisable(true);
    setBackHidden(false);
    if (tab === 1) {
      setBackHidden(true);
    }
    if (tab === 4) {
      setProcessDisable(false);
      setBackHidden(false);
    }
  };

  const onBackClick = () => {
    let tab = +selectedTab - 1;
    setSelectedTab(tab.toString());
    setProcessDisable(true);
    setBackHidden(false);
    if (tab === 1) {
      setBackHidden(true);
    }
  };

  const handleTabChange = (
    event: ChangeEvent<HTMLInputElement>,
    newValue: number
  ) => {
    let selectTab = +selectedTab;
    if (selectedTab === "4") {
      setProcessDisable(false);
    }
    if (newValue <= selectTab) {
      setSelectedTab(newValue.toString());
      setBackHidden(true);
    }
    if (selectedTab === "1") {
      setBackHidden(false);
    }
  };

  const handleCPUChange = (core: ICpuInterface, configurationLabel: string) => {
    let imageCoast: ICostInterface = {
      title: `Core -  ${core.label}`,
      cost: core.price,
      id: 2,
    };
    setCostValue(imageCoast);
    setSelectCore(core);
    setConfigurationLabel(configurationLabel);
  };

  const handleMemoryChange = (
    memory: ICpuInterface,
    configurationLabel: string
  ) => {
    let imageCoast: ICostInterface = {
      title: `Memory -  ${memory.label}`,
      cost: memory.price,
      id: 3,
    };
    setCostValue(imageCoast);
    setSelectMemory(memory);
    setConfigurationLabel(configurationLabel);
  };

  const setProcessBtnAfterConfiuration = (updateList: ICostInterface[]) => {
    let cpuIndex = updateList.findIndex((element) => element.id === 2);
    let memoryIndex = updateList.findIndex((element) => element.id === 3);
    if (cpuIndex !== -1 && memoryIndex !== -1) {
      setProcessDisable(false);
    }
  };

  const handleStorageNetworkChange = (
    storageArray: IStorageInterface[],
    enbale: boolean
  ) => {
    storageArray.forEach((element) => {
      element.isreview = true;
    });
    setStorageArray(storageArray);
    setProcessDisable(enbale);

    let scost = 0;
    storageArray.forEach((e, i) => {
      scost += e.price;
    });
    let socost: ICostInterface = {
      title: `Storage`,
      cost: scost,
      id: 5,
    };
    setCostValue(socost);
  };

  const handleBandwidthChange = (bandwidthparam: number) => {
    let unit,
      units = ["TB", "GB"];
    let convertband = 0;
    for (
      unit = units.pop();
      units.length && bandwidthparam >= 1024;
      unit = units.pop()
    ) {
      convertband = bandwidthparam / 1024;
    }

    if (unit === "GB") {
      let localbandwidth: string = `${convertband.toFixed()}${unit}`;
      setBandwidth(localbandwidth);
    } else {
      let localbandwidth: string = `${convertband.toFixed(1)}${unit}`;
      setBandwidth(localbandwidth);
    }
    console.log(bandwidthparam);
    console.log(bandwidth);
    let price = 5;
    if (bandwidthparam < 512) {
      price = 5;
    } else if (bandwidthparam >= 512 && bandwidthparam < 1024) {
      price = 10;
    } else if (bandwidthparam >= 1024 && bandwidthparam < 1536) {
      console.log(bandwidthparam);
      price = 15;
    } else {
      price = 20;
    }
    let imageCoast: ICostInterface = {
      title: `Bandwidth`,
      cost: price,
      id: 4,
    };
    setCostValue(imageCoast);
  };

  const onEditClick = (tabno: number) => {
    setSelectedTab(tabno.toString());
    setBackHidden(false);
    setProcessDisable(true);
  };

  return (
    <div>
      <header className="header sticky">
        <span className="titleText">
          <b>HVC</b>
        </span>
      </header>
      <section className="maindiv">
        <div className="subdiv">
          <div className="titlediv">
            <span>Choose Image</span>
            <DropdownComponent
              label="Regions"
              items={regions}
              selectedItem={selectedRegion}
              onSlectionChange={onSlectionChange}
            />
          </div>
          <TabContainer
            selectedTab={selectedTab}
            selectedRegion={selectedRegion}
            onImageSlectionChange={onImageSlectionChange}
            handleTabChange={handleTabChange}
            handleCPUChange={handleCPUChange}
            handleMemoryChange={handleMemoryChange}
            selectedImage={selectedImage}
            selectCore={selectCore}
            selectMemory={selectMemory}
            configurationLabel={configurationLabel}
            handleStorageNetworkChange={handleStorageNetworkChange}
            handleBandwidthChange={handleBandwidthChange}
            bandwidth={bandwidth}
            storageArray={storageArray}
            onEditClick={onEditClick}
          />
        </div>
        <aside>
          <CostComponent costEstimate={costEstimate} totalPrice={TotalPrice} />
        </aside>
      </section>
      <footer className="footerclass">
        <span hidden={backHidden}>
          <Button
            variant="contained"
            color="inherit"
            className="btnclass"
            onClick={onBackClick}
          >
            Back
          </Button>
        </span>
        <Button
          variant="contained"
          color="primary"
          className="btnclass"
          onClick={onProceedClick}
          disabled={processDisable}
        >
          Proceed
        </Button>
      </footer>
    </div>
  );
}

export default MainContainer;
