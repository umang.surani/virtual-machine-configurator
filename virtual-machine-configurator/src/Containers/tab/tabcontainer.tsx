import { Paper, Tab, Tabs } from "@material-ui/core";
import React from "react"; 
import { ChooseImageComponent } from "../../Components/chooseimage/chooseimage"; 
import { TabPanel } from "../../Components/tabpanel/tabpanel";
import { InstaceTypeContainer } from "../instancetype/instacetypecontainer";
import { StorageNetworkContainer } from "../storagenetwork/storagenetworkcontainer";
import { SummaryContainer } from "../summary/summarycontainer";
import './tab.css'

export function TabContainer(prop: any) {
    return (
        <Paper square className="pageclass">
            <Tabs value={prop?.selectedTab} indicatorColor="primary" textColor="primary" 
                  onChange={prop?.handleTabChange} aria-label="disabled tabs example">
                <Tab label="Choose Image" value="1" tabIndex="1"/> 
                <Tab label="Choose Instance type" value="2" tabIndex="2"/>
                <Tab label="Choose Storage and Netwrok" value="3" tabIndex="3"/>
                <Tab label="Review & Launch" value="4" tabIndex="4"/> 
            </Tabs>
            <TabPanel value={prop?.selectedTab} index="1">
               <ChooseImageComponent selectedRegion={prop?.selectedRegion} 
                                     onImageSlectionChange= {prop?.onImageSlectionChange}/>
            </TabPanel>
            <TabPanel value={prop?.selectedTab} index="2">
                <InstaceTypeContainer {...prop}/>
            </TabPanel >
            <TabPanel value={prop?.selectedTab} index="3">
               <StorageNetworkContainer  {...prop}/>
            </TabPanel >
            <TabPanel value={prop?.selectedTab} index="4">
                <SummaryContainer {...prop}/>
            </TabPanel >
        </Paper>
    )

}