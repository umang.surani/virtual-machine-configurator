import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  Tab,
  Tabs,
} from "@material-ui/core";
import React, { useState } from "react";
import DropdownComponent from "../../Components/DropDown/dropdowncomponent";
import { ICpuInterface } from "../../interface/cpuinterface";
import "./instacetype.css";

const CPUCore: ICpuInterface[] = [
  {
    label: "1 Core",
    value: 1,
    price: 5.0,
    id: 1,
  },
  {
    label: "2 Core",
    value: 2,
    price: 10.0,
    id: 2,
  },
  {
    label: "4 Core",
    value: 4,
    price: 20.0,
    id: 4,
  },
  {
    label: "8 Core",
    value: 8,
    price: 40.0,
    id: 8,
  },
  {
    label: "16 Core",
    value: 16,
    price: 100.0,
    id: 16,
  },
];

const Memory: ICpuInterface[] = [
  {
    label: "256 MB",
    value: 256,
    price: 5.0,
    id: 256,
  },
  {
    label: "512 MB",
    value: 512,
    price: 10.0,
    id: 512,
  },
  {
    label: "1 GB",
    value: 1,
    price: 20.0,
    id: 1,
  },
  {
    label: "2 GB",
    value: 2,
    price: 40.0,
    id: 2,
  },
  {
    label: "4 GB",
    value: 4,
    price: 100.0,
    id: 4,
  },
  {
    label: "8 GB",
    value: 8,
    price: 100.0,
    id: 8,
  },
  {
    label: "16 GB",
    value: 16,
    price: 200.0,
    id: 16,
  },
  {
    label: "32 GB",
    value: 32,
    price: 420.0,
    id: 32,
  },
  {
    label: "64 GB",
    value: 64,
    price: 880.0,
    id: 64,
  },
];

const GPCPU = [1, 2, 4];
const SOCPU = [1, 8, 16];
const CMCPU = [1, 2, 8, 16];
const NWCPU = [1, 2, 4, 8, 16];

const GPMemory = [256, 512, 1, 2, 4];
const SOMemory = [16, 32, 64];
const CMMemory = [16, 32, 64];
const NWMemory = [256, 512, 1, 2, 4, 16, 32, 64];

export function InstaceTypeContainer(prop: any) {
  const InitCPU = () => {
    let CPUItems: ICpuInterface[] = [];
    let cpuDefalut: ICpuInterface = {
      label: "CPU Core",
      value: 0,
      price: 0,
      id: 0,
    };
    CPUItems.push(cpuDefalut);
    GPCPU.forEach((c) => {
      CPUItems.push(CPUCore.find((cp) => cp.id === c) as ICpuInterface);
    });
    return CPUItems;
  };

  const InitMemory = () => {
    let MemoryItems: ICpuInterface[] = [];
    let memoryDefault: ICpuInterface = {
      label: "Memory",
      value: 0,
      price: 0,
      id: 0,
    };
    MemoryItems.push(memoryDefault);
    GPMemory.forEach((c) => {
      MemoryItems.push(Memory.find((cp) => cp.id === c) as ICpuInterface);
    });
    return MemoryItems;
  };

  const [value, setValue] = useState("1");
  const [cpuItems, setcpuItems] = useState(InitCPU);
  const [memoryItem, setMemoryItems] = useState(InitMemory);
  const [selectedCPU, setSelectedCPU] = useState(0);
  const [selectedMemory, setSelectedMemory] = useState(0);
  const [open, setOpen] = React.useState(false);
  const [tempSelectedTab, setTempSelectedTab] = React.useState("0");

  const handleClickOpen = () => {
    setValue(tempSelectedTab);
    let CPUItems: ICpuInterface[] = [];
    let MemoryItems: ICpuInterface[] = [];
    let cpuDefalut: ICpuInterface = {
      label: "CPU Core",
      value: 0,
      price: 0,
      id: 0,
    };
    CPUItems.push(cpuDefalut);
    let memoryDefault: ICpuInterface = {
      label: "Memory",
      value: 0,
      price: 0,
      id: 0,
    };
    MemoryItems.push(memoryDefault);
    if (tempSelectedTab === "1") {
      GPCPU.forEach((c) => {
        CPUItems.push(CPUCore.find((cp) => cp.id === c) as ICpuInterface);
      });
      GPMemory.forEach((c) => {
        MemoryItems.push(Memory.find((cp) => cp.id === c) as ICpuInterface);
      });
      setSelectedCPU(GPCPU[0]);
      setSelectedMemory(GPMemory[0]);
    }
    if (tempSelectedTab === "2") {
      SOCPU.forEach((c) => {
        CPUItems.push(CPUCore.find((cp) => cp.id === c) as ICpuInterface);
      });
      SOMemory.forEach((c) => {
        MemoryItems.push(Memory.find((cp) => cp.id === c) as ICpuInterface);
      });
      setSelectedCPU(SOCPU[0]);
      setSelectedMemory(SOMemory[0]);
    }
    if (tempSelectedTab === "3") {
      CMCPU.forEach((c) => {
        CPUItems.push(CPUCore.find((cp) => cp.id === c) as ICpuInterface);
      });
      CMMemory.forEach((c) => {
        MemoryItems.push(Memory.find((cp) => cp.id === c) as ICpuInterface);
      });
      setSelectedCPU(CMCPU[0]);
      setSelectedMemory(CMMemory[0]);
    }
    if (tempSelectedTab === "4") {
      NWCPU.forEach((c) => {
        CPUItems.push(CPUCore.find((cp) => cp.id === c) as ICpuInterface);
      });
      NWMemory.forEach((c) => {
        MemoryItems.push(Memory.find((cp) => cp.id === c) as ICpuInterface);
      });
      setSelectedCPU(NWCPU[0]);
      setSelectedMemory(NWMemory[0]);
    }
    setMemoryItems(MemoryItems);
    setcpuItems(CPUItems);
    setOpen(false);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleChange = (event: React.ChangeEvent<{}>, newValue: string) => {
    setTempSelectedTab(newValue);
    setOpen(true);
  };

  const handleCPUChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const val: number = +event?.target?.value.toString();
    setSelectedCPU(val);
    if (val !== 0) {
      let core = cpuItems.find((cp) => cp.id === val) as ICpuInterface;
      prop.handleCPUChange(core,setLablel(val));
    }
  };

  const handleMemoryChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const val: number = +event?.target?.value.toString();
    setSelectedMemory(val);
    if (val !== 0) {
      let memory = memoryItem.find((cp) => cp.id === val) as ICpuInterface;
      prop.handleMemoryChange(memory, setLablel(val));
    }
  };

  const setLablel = (val: number)=> {
    if(val === 1) {
      return "General Purpose";
    } else if(val === 2) {
      return "CPU Optimised";      
    } else if(val === 3) {
      return "Storage Optimised";      
    } else if(val === 4) {
      return "Network Optimised";    }
  }

  return (
    <div className="instacecontainer">
      <Tabs
        value={value}
        indicatorColor="primary"
        textColor="primary"
        onChange={handleChange}
        aria-label="disabled tabs example"
      >
        <Tab label="General Purpose" value="1" />
        <Tab label="CPU Optimised" value="2" />
        <Tab label="Storage Optimised" value="3" />
        <Tab label="Network Optimised" value="4" />
      </Tabs>
      <h2>Create Configuration</h2>
      <span className="instacecontent">
        <span className="instacedropdown">
          <DropdownComponent
            label="CPU Core"
            items={cpuItems}
            selectedItem={selectedCPU}
            onSlectionChange={handleCPUChange}
          />
        </span>
        <span className="instacedropdown">
          <DropdownComponent
            label="Memory"
            items={memoryItem}
            selectedItem={selectedMemory}
            onSlectionChange={handleMemoryChange}
          />
        </span>
      </span>
      <Dialog
        open={open}
        onClose={handleClose}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description"
      >
        <DialogTitle id="alert-dialog-title">{"Warning"}</DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-description">
            Current selected storage configuration is incompatible with the
            instance type you selected, Selecting this type will lead to losing
            all storage configuration. Are you sure, you want to change instance
            type.
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            No
          </Button>
          <Button onClick={handleClickOpen} color="primary" autoFocus>
            Yes
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
